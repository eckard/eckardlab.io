---
title: Unicode combining characters
subtitle: How many characters are in "Köln"?
date: 2021-04-25
tags: ["java","unicode"]
---

Of course the word "Köln" contains 4 chracters, right?

But be aware of the umlaut! It can be represented in different ways in unicode.

<!--more-->
### Combining characters

The character "ö" can be represented as 'ö' (U+00f6) or as 'o' (U+006f) followed by '◌̈' (U+0308),
which is a so called "combining character" or in this case more sepcifically a "combining diacritical mark".

{{< highlight java "linenos=inline">}}
final String germanCity = "Köln";
assertThat(germanCity.length()).isEqualTo(4);
System.out.println(detailString(germanCity));

final String germanCity2 = "Köln";
assertThat(germanCity2).isNotEqualTo(germanCity);
assertThat(germanCity2.length()).isEqualTo(5);
System.out.println(detailString(germanCity2));
{{</ highlight >}}



### Normalizing

Normalizing

### Not "surrogate pairs"

This is not a question of unicode characters that are represented as a pair of Java chars.