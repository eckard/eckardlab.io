---
title: About me
subtitle: What I do and did
comments: false
---

My name is Eckard Mühlich.

- I'm the father of two wonderful daughters
- I'm married to an amazing woman
- I live in Munich
- I'm developing software for CHECK24


### my history

- I was born in Nellingen auf den Fildern
- I grew up in Reutlingen
- I studied in Reutlingen
